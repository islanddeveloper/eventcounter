﻿//-----------------------------------------------------------------------------
// <copyright file="EventCounterTests.cs">
//   Author: Mark Dolden
// </copyright>

// Description:
//      Class for handling the Event Counter Interface
//-----------------------------------------------------------------------------

using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RC.CodingChallenge;

namespace EventCounterTests
{
    /// <summary>
    /// Implementation of the IEventCounter Interface
    /// </summary>
    [TestClass]
    public class EventCounterTests
    {
        IEventCounter _eventCounter;

        [TestInitialize]
        public void  StartUp()
        {
            _eventCounter = new EventCounter();
        }

        /// <summary>
        /// Simple Happy Path Test
        /// </summary>
        [TestMethod]
        public void SimpleEventTest()
        {
            StreamReader sr = GetLogs(@"..\..\SampleData\EventCounter-HappyPath.csv");
            _eventCounter.ParseEvents("1000", sr);
            Assert.AreEqual(1, _eventCounter.GetEventCount("1000"));
        }

        /// <summary>
        /// make it almost a fault but instead of finishing at state0 it finishes at state1 so no fault
        /// </summary>
        [TestMethod]
        public void Stage1NoFault()
        {
            StreamReader sr = GetLogs(@"..\..\SampleData\EventCounter-State1NoFault.csv");
            _eventCounter.ParseEvents("1001", sr);
            Assert.AreEqual(0, _eventCounter.GetEventCount("1001"));
        }

        /// <summary>
        /// make it almost a fault except that state3 is less than 5 minutes
        /// </summary>
        [TestMethod]
        public void Stage3LessThan5Minutes()
        {
            StreamReader sr = GetLogs(@"..\..\SampleData\EventCounter-State3LessThan5Minutes.csv");
            _eventCounter.ParseEvents("1002", sr);
            Assert.AreEqual(0, _eventCounter.GetEventCount("1002"));
        }

        /// <summary>
        /// check to make sure state3 for greater than 5 minutes indicates a possible fault
        /// </summary>
        [TestMethod]
        public void Stage3GreaterThan5Minutes()
        {
            StreamReader sr = GetLogs(@"..\..\SampleData\EventCounter-State3GreaterThan5Minutes.csv");
            _eventCounter.ParseEvents("1003", sr);
            Assert.AreEqual(1, _eventCounter.GetEventCount("1003"));
        }

        /// <summary>
        /// check that EventCounter can handle multiple faults in one log
        /// </summary>
        [TestMethod]
        public void MultipleFaults()
        {
            StreamReader sr = GetLogs(@"..\..\SampleData\EventCounter-MultipleFaults.csv");
            _eventCounter.ParseEvents("1004", sr);
            Assert.AreEqual(3, _eventCounter.GetEventCount("1004"));
        }

        /// <summary>
        /// check that EventCounter can handle multiple redundant heartbeat measurements
        /// </summary>
        [TestMethod]
        public void MultipleRedundantHeartBeats()
        {
            StreamReader sr = GetLogs(@"..\..\SampleData\EventCounter-MultipleRedundantHeartBeats.csv");
            _eventCounter.ParseEvents("1005", sr);
            Assert.AreEqual(1, _eventCounter.GetEventCount("1005"));
        }

        /// <summary>
        /// check the case with multiple stage 3 with a short duration
        /// </summary>
        [TestMethod]
        public void RedudantHeartBeatsStage3ShortDuration()
        {
            StreamReader sr = GetLogs(@"..\..\SampleData\EventCounter-Stage3ShortDuration.csv");
            _eventCounter.ParseEvents("1009", sr);
            Assert.AreEqual(1, _eventCounter.GetEventCount("1009"));
        }

        /// <summary>
        /// check that state3 direct to state0 is fault
        /// </summary>
        [TestMethod]
        public void Stage2DirectToStage0NoFault()
        {
            StreamReader sr = GetLogs(@"..\..\SampleData\EventCounter-Stage2DirectToStage0NoFault.csv");
            _eventCounter.ParseEvents("1010", sr);
            Assert.AreEqual(0, _eventCounter.GetEventCount("1010"));
        }

        /// <summary>
        /// check that state3 direct to state0 is fault
        /// </summary>
        [TestMethod]
        public void Stage3DirectToStage0NoFault()
        {
            StreamReader sr = GetLogs(@"..\..\SampleData\EventCounter-State3DirectToState0Fault.csv");
            _eventCounter.ParseEvents("1006", sr);
            Assert.AreEqual(0, _eventCounter.GetEventCount("1006"));
        }

        /// <summary>
        /// check that state3 direct to state1 is no fault
        /// </summary>
        [TestMethod]
        public void Stage3DirectToStage1NoFault()
        {
            StreamReader sr = GetLogs(@"..\..\SampleData\EventCounter-State3DirectToState1NoFault.csv");
            _eventCounter.ParseEvents("1007", sr);
            Assert.AreEqual(0, _eventCounter.GetEventCount("1007"));
        }

        /// <summary>
        /// check the performance of parsing a large log
        /// </summary>
        [TestMethod]
        public void LargeLogPerformanceTest()
        {
            StreamReader sr = GetLogs(@"..\..\SampleData\EventCounter-LargeLogPerformanceTest.csv");
            _eventCounter.ParseEvents("1008", sr);
            Assert.AreEqual(7, _eventCounter.GetEventCount("1008"));
        }

        /// <summary>
        /// Simple Happy Path Test
        /// </summary>
        [TestMethod]
        public void CombinedStage3Test()
        {
            StreamReader sr = GetLogs(@"..\..\SampleData\EventCounter-CombinedStage3GreaterThan5Minutes1Fault.csv");
            _eventCounter.ParseEvents("1010", sr);
            Assert.AreEqual(1, _eventCounter.GetEventCount("1010"));
        }

        /// <summary>
        /// check that the event counter can parse logs from multiple threads
        /// </summary>
        [TestMethod]
        public void MultiThreadedTest()
        {
            StreamReader sr1 = GetLogs(@"..\..\SampleData\EventCounter-MultiThread1.csv");
            StreamReader sr2 = GetLogs(@"..\..\SampleData\EventCounter-MultiThread2.csv");
            StreamReader sr3 = GetLogs(@"..\..\SampleData\EventCounter-MultiThread3.csv");
            StreamReader sr4 = GetLogs(@"..\..\SampleData\EventCounter-MultiThread4.csv");
            StreamReader sr5 = GetLogs(@"..\..\SampleData\EventCounter-MultiThread5.csv");
            StreamReader sr6 = GetLogs(@"..\..\SampleData\EventCounter-MultiThread6.csv");
            StreamReader sr7 = GetLogs(@"..\..\SampleData\EventCounter-MultiThread7.csv");
            StreamReader sr8 = GetLogs(@"..\..\SampleData\EventCounter-MultiThread8.csv");

            Task[] prodCons = new Task[] {
                Task.Run(() => _eventCounter.ParseEvents("1011", sr1)),
                Task.Run(() => _eventCounter.ParseEvents("1011", sr2)),
                Task.Run(() => _eventCounter.ParseEvents("1011", sr3)),
                Task.Run(() => _eventCounter.ParseEvents("1011", sr4)),
                Task.Run(() => _eventCounter.ParseEvents("1011", sr5)),
                Task.Run(() => _eventCounter.ParseEvents("1011", sr6)),
                Task.Run(() => _eventCounter.ParseEvents("1011", sr7)),
                Task.Run(() => _eventCounter.ParseEvents("1011", sr8)),
            };

            Task.WaitAll(prodCons);

            Assert.AreEqual(16, _eventCounter.GetEventCount("1011"));
        }

        #region helper methods
        /// <summary>
        /// Gets the Log for a given path
        /// </summary>
        /// <param name="path">path of the log file</param>
        /// <returns>a StreamReader containing the log entries</returns>
        public static StreamReader GetLogs(string path)
        {
            StreamReader sr = null;
            try
            {
                if (!string.IsNullOrEmpty(path))
                {
                    sr = new StreamReader(path);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }

            return sr;
        }
        #endregion
    }
}