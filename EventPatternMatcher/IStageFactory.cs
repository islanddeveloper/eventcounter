﻿using RC.CodingChallenge.Contracts;
using RC.CodingChallenge.Stages;

namespace RC.CodingChallenge
{
    /// <summary>
    /// Interface used to construct stages
    /// </summary>
    public interface IStageFactory
    {
        /// <summary>
        /// Create and Initialize stages
        /// </summary>
        /// <param name="eventCounter">used to initialize stages</param>
        /// <param name="currentEvent">used to initialize stages</param>
        StageOperationBase MakeStage(EventCounter eventCounter, Event currentEvent);
    }
}