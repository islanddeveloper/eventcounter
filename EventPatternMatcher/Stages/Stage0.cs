﻿using System.Collections.Concurrent;

namespace RC.CodingChallenge.Stages
{
    /// <summary>
    /// functionality for handling Stage0 operations
    /// </summary>
    sealed class Stage0 : StageOperationBase
    {
        /// <summary>
        /// Used to hold device, error records
        /// </summary>
        private readonly ConcurrentDictionary<string, int> _deviceErrors;

        /// <summary>
        /// Initializes a new instance of the <see cref="Stage0"/> class.
        /// </summary>
        /// <param name="possibleFault">A flag indicating possible fault</param>
        /// <param name="hardwareErrors">A lookup for device errors</param>
        internal Stage0(bool possibleFault, ConcurrentDictionary<string, int> hardwareErrors) : base(possibleFault)
        {
            _deviceErrors = hardwareErrors;
        }

        /// <summary>
        /// Sets the fault condition
        /// </summary>
        /// <returns>the possibility of a hardware fault</returns>
        internal override bool SetFaultCondition()
        {
            if (IsPossibleFault)
            {
                // Fault detected increment count
                _deviceErrors.AddOrUpdate(CurrentEvent.DeviceId, 1, (key, oldValue) => oldValue + 1);

                // Reset fault flag for the next event
                IsPossibleFault = false;
            }

            ResetElapsedTime();

            return IsPossibleFault;
        }
    }
}