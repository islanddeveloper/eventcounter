﻿namespace RC.CodingChallenge.Stages.Stage3Time
{
    /// <summary>
    /// Interface for Stage3ElapsedTime Behaviour
    /// </summary>
    public interface IStage3ElapsedTime
    {
        bool CheckForFault(int eventType, bool possiblefault);
    }
}