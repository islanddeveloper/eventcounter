﻿using RC.CodingChallenge.Utilities;

namespace RC.CodingChallenge.Stages.Stage3Time
{
    /// Concrete implementation of Stage3ElapsedTimeBase used to determine fault condition 
    /// when previous event and the next event timespan is less than 5 minutes
    sealed class LessThan5Minutes : IStage3ElapsedTime
    {
        private readonly double _stage3ElapsedTime;

        /// <summary>
        /// Creates a new instance of the LessThan5Minutes class
        /// </summary>
        internal LessThan5Minutes(double stage3ElapsedTime)
        {
            _stage3ElapsedTime = stage3ElapsedTime;
        }

        /// <summary>
        /// Determines fault when stage 3 is less than 5 minutes
        /// </summary>
        public bool CheckForFault(int eventType, bool ispossiblefault)
        {
            if (_stage3ElapsedTime >= Constants.FiveMinutes && eventType == Constants.Stage2)
            {
                ispossiblefault = true;
            }

            return ispossiblefault;
        }
    }
}