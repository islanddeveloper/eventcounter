﻿using RC.CodingChallenge.Utilities;

namespace RC.CodingChallenge.Stages.Stage3Time
{
    /// <summary>
    /// Concrete implementation of Stage3ElapsedTimeBase used to determine fault condition 
    /// when previous event and the next event timespan is greater than 5 minutes
    /// </summary>
    sealed class GreaterThanOrEqualTo5Minutes : IStage3ElapsedTime
    {
        /// <summary>
        /// Determines fault when stage 3 is greater than 5 minutes
        /// </summary>
        public bool CheckForFault(int eventType, bool ispossiblefault)
        {
            if (eventType == Constants.Stage2)
            {
                // possible error condition
                ispossiblefault = true;
            }

            return ispossiblefault;
        }
    }
}