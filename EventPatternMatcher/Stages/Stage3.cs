﻿using System;
using RC.CodingChallenge.Contracts;
using RC.CodingChallenge.Queue;
using RC.CodingChallenge.Stages.Stage3Time;
using RC.CodingChallenge.Utilities;

namespace RC.CodingChallenge.Stages
{
    /// <summary>
    /// functionality for handling Stage3 operations
    /// </summary>
    sealed class Stage3 : StageOperationBase
    {
        private IStage3ElapsedTime _elapsedTimeStrategy;
        private double _elapsedTime;
        private readonly IEventQueue _eventQueue;

        /// <summary>
        /// Initializes a new instance of the <see cref="Stage3"/> class.
        /// </summary>
        /// <param name="possibleFault">A flag indicating possible fault</param>
        /// <param name="queue">/// A queue of hardware events used to peek ahead at the future events</param>
        internal Stage3(bool possibleFault, IEventQueue queue) : base(possibleFault)
        {
            _eventQueue = queue;
        }

        /// <summary>
        /// Sets the fault condition
        /// </summary>
        /// <returns>the possibility of a hardware fault</returns>
        internal override bool SetFaultCondition()
        {
            try
            {
                NextEvent = _eventQueue.Peek();

                _elapsedTime = CalculateElapsedTime(CurrentEvent, NextEvent);

                SetStage3TimeStrategy(_elapsedTime);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }

            return _elapsedTimeStrategy.CheckForFault(NextEvent.EventType, IsPossibleFault);
        }

        /// <summary>
        /// Used to determine behaviour of Stage3 internal logic
        /// </summary>
        /// <returns>The elapsed time between two events</returns>
        private static double CalculateElapsedTime(Event currentevent, Event nextEvent)
        {
            return (nextEvent.EventDate - currentevent.EventDate).TotalSeconds;
        }

        /// <summary>
        /// Sets the behaviour of Stage3 internal logic
        /// </summary>
        /// <returns>the possibility of a hardware fault</returns>
        private void SetStage3TimeStrategy(double elapsedtime)
        {
            if (elapsedtime >= Constants.FiveMinutes)
            {
                _elapsedTimeStrategy = new GreaterThanOrEqualTo5Minutes();
            }
            else
            {
                SetElapsedTime();

                if (NextEvent.EventType == Constants.Stage2)
                    Stage3OverallElapsedTime += _elapsedTime;

                _elapsedTimeStrategy = new LessThan5Minutes(Stage3OverallElapsedTime);
            }
        }
    }
}