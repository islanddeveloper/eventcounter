﻿namespace RC.CodingChallenge.Stages
{
    /// <summary>
    /// functionality for handling Stage1 operations
    /// </summary>
    sealed class Stage1 : StageOperationBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Stage1"/> class.
        /// </summary>
        internal Stage1() : base(false)
        {
        }

        /// <summary>
        /// Sets the fault condition
        /// </summary>
        /// <returns>the possibility of a hardware fault</returns>
        internal override bool SetFaultCondition()
        {
            ResetElapsedTime();

            return false;
        }
    }
}