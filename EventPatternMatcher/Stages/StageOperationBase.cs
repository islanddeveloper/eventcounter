﻿using RC.CodingChallenge.Contracts;
using RC.CodingChallenge.Utilities;

namespace RC.CodingChallenge.Stages
{
    /// <summary>
    /// Abstract base class for handling event stages
    /// </summary>
    public abstract class StageOperationBase
    {
        public bool IsPossibleFault { get; set; }
        public Event NextEvent { get; set; } = new Event();
        public Event CurrentEvent { get; set; } = new Event();
        public Event PreviousEvent { get; set; } = new Event();
        protected static double Stage3OverallElapsedTime { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="StageOperationBase" /> class.
        /// </summary>
        /// <param name="ispossiblefault">A flag indicating possible fault</param>
        protected StageOperationBase(bool ispossiblefault)
        {
            IsPossibleFault = ispossiblefault;
        }

        protected void SetElapsedTime()
        {
            if (PreviousEvent.EventType == Constants.Stage3)
            {
                Stage3OverallElapsedTime += (CurrentEvent.EventDate - PreviousEvent.EventDate).TotalSeconds;
            }
        }

        protected void ResetElapsedTime()
        {
            Stage3OverallElapsedTime = 0;
        }

        /// <summary>
        /// sets the fault conditions for the given stage
        /// </summary>
        /// <returns>the possibility of a hardware fault</returns>
        internal abstract bool SetFaultCondition();
    }
}