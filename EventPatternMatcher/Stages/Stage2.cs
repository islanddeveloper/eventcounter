﻿namespace RC.CodingChallenge.Stages
{
    /// <summary>
    /// functionality for handling Stage2 operations
    /// </summary>
    sealed class Stage2 : StageOperationBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Stage2"/> class.
        /// </summary>
        /// <param name="possibleFault">A flag indicating possible fault</param>
        internal Stage2(bool possibleFault) : base(possibleFault)
        {
        }

        /// <summary>
        /// Sets the fault condition
        /// </summary>
        /// <returns>the possibility of a hardware fault</returns>
        internal override bool SetFaultCondition()
        {
            return IsPossibleFault;
        }
    }
}