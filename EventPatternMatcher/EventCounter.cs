﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading;
using RC.CodingChallenge.Contracts;
using RC.CodingChallenge.Queue;
using RC.CodingChallenge.Stages;

namespace RC.CodingChallenge
{
    /// <summary>
    /// Implementation of the IEventCounter Interface
    /// </summary>
    public class EventCounter : IEventCounter, IDisposable
    {
        /// <summary>
        /// Used to scyhronize threads
        /// </summary>
        private AutoResetEvent _syncThreads = new AutoResetEvent(true);

        /// <summary>
        /// Used to hold device error counts
        /// </summary>
        public ConcurrentDictionary<string, int> DeviceErrors { get; set; } = new ConcurrentDictionary<string, int>();

        /// <summary>
        /// Queue of Hardware Events
        /// </summary>
        private readonly IEventQueue _sourceQueue = new EventQueue();
        public IEventQueue SourceQueue => _sourceQueue;

        /// <summary>
        /// Gets or sets the curent <see cref="Event"/> to use for its value and timestamp.
        /// </summary>
        private Event _currentEvent;
        public Event CurrentEvent => _currentEvent;

        /// <summary>
        /// An object to the abstract base class for events
        /// </summary>
        private StageOperationBase _stageOperationBase;

        /// <summary>
        /// Gets the count of failures for a given device
        /// </summary>
        /// <param name="deviceId">Id of the hardware device</param>
        /// <returns>fault count per device</returns>
        public int GetEventCount(string deviceId)
        {
            if (string.IsNullOrEmpty(deviceId))
            {
                throw new ArgumentNullException(deviceId + " is must contain a valid deviceId");
            }

            if (DeviceErrors.TryGetValue(deviceId, out var result))
            {
                return result;
            }

            return 0;
        }

        /// <summary>
        /// Parses the event log
        /// </summary>
        /// <param name="deviceId">Id of the hardware device</param>
        /// <param name="eventLog">log of events</param>
        public void ParseEvents(string deviceId, StreamReader eventLog)
        {
            try
            {
                if (string.IsNullOrEmpty(deviceId)) { throw new ArgumentNullException(); }
                if (eventLog == null) { throw new ArgumentNullException(); }

                _syncThreads.WaitOne();
                using (eventLog)
                {
                    string line;
                    char[] delimiters = new char[] { '\t' };

                    while ((line = eventLog.ReadLine()) != null)
                    {
                        string[] eventParts = line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        if (eventParts[0].ToUpper() == "TIMESTAMP")
                        {
                            continue;
                        }

                        Event deviceEvent = new Event
                        {
                            EventDate = Convert.ToDateTime(eventParts[0]),
                            EventType = Convert.ToInt32(eventParts[1]),
                            DeviceId = deviceId
                        };

                        _sourceQueue.Add(deviceEvent);
                    }
                }

                EnumerateEvents();
                _syncThreads.Set();
            }
            catch (Exception ex)
            {
                _syncThreads.Set();
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// EnumerateEvents
        /// </summary>
        private void EnumerateEvents()
        {
            _currentEvent = _sourceQueue.GetNext();

            if (_currentEvent == null)
            {
                return;
            }

            IStageFactory factory = new StageFactory();
            Event previousevent = new Event();

            while (_currentEvent != null)
            {
                _stageOperationBase = factory.MakeStage(this, _currentEvent);

                _stageOperationBase.PreviousEvent = previousevent;
                _stageOperationBase.CurrentEvent = _currentEvent;
                _currentEvent.IsPossibleDeviceFault = _stageOperationBase.SetFaultCondition();
                previousevent = _currentEvent;

                _currentEvent = _sourceQueue.GetNext();

                if (_currentEvent != null)
                {
                    _currentEvent.IsPossibleDeviceFault = previousevent.IsPossibleDeviceFault;
                }
            }
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose
        /// </summary>
        /// <param name="disposing">dispose flag</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources 
                // https://msdn.microsoft.com/en-us/library/ms244737(v=vs.80).aspx
                if (_syncThreads != null)
                {
                    _syncThreads.Dispose();
                    _syncThreads = null;
                }
            }
        }
    }
}