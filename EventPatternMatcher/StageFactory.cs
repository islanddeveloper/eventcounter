﻿using System;
using RC.CodingChallenge.Contracts;
using RC.CodingChallenge.Stages;
using RC.CodingChallenge.Utilities;

namespace RC.CodingChallenge
{
    /// <summary>
    /// Factory class used to create stages
    /// </summary>
    public class StageFactory : IStageFactory
    {
        /// <summary>
        /// Constructs the given stage
        /// </summary>
        /// <param name="counter">An instance of the <see cref="StageFactory"/> class used to intialize the stages</param>
        /// <param name="currentEvent">current event used to track hardware faults</param>
        public StageOperationBase MakeStage(EventCounter counter, Event currentEvent)
        {
            try
            {
                switch (counter.CurrentEvent.EventType)
                {
                    case Constants.Stage0:
                        return new Stage0(currentEvent.IsPossibleDeviceFault, counter.DeviceErrors);
                    case Constants.Stage1:
                        return new Stage1();
                    case Constants.Stage2:
                        return new Stage2(currentEvent.IsPossibleDeviceFault);
                    case Constants.Stage3:
                        return new Stage3(currentEvent.IsPossibleDeviceFault, counter.SourceQueue);
                    default:
                        throw new ArgumentException($"Unknown event type {counter.CurrentEvent.EventType}");
                }
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }
    }
}