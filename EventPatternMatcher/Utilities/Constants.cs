﻿namespace RC.CodingChallenge.Utilities
{
    /// <summary>
    /// Constants used by the Event counter
    /// </summary>
    public static class Constants
    {
        internal const int Stage0 = 0;
        internal const int Stage1 = 1;
        internal const int Stage2 = 2;
        internal const int Stage3 = 3;
        internal const int FiveMinutes = 300;
    }
}