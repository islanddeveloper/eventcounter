﻿using System;
using System.Collections.Concurrent;
using RC.CodingChallenge.Contracts;

namespace RC.CodingChallenge.Queue
{
    /// <summary>
    /// Provides the functionality for working with the queue of hardware events
    /// </summary>
    sealed class EventQueue : IEventQueue
    {
        /// <summary>
        /// Queue to hold events
        /// </summary>
        private readonly ConcurrentQueue<Event> _eventQueue = new ConcurrentQueue<Event>();

        /// <summary>
        /// Used to hold the next event
        /// </summary>
        private Event _nextEvent = new Event();

        /// <summary>
        /// Provides functionality to add an event to the queue
        /// </summary>
        public void Add(Event hardwareEvent)
        {
            try
            {
                _eventQueue.Enqueue(hardwareEvent);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        /// <summary>
        /// Provides functionality to get the next event from the queue
        /// </summary>
        public Event GetNext()
        {
            if (_eventQueue.IsEmpty)
            {
                return null;
            }

            if(!_eventQueue.TryDequeue(out _nextEvent))
            {
                Console.WriteLine("Failed getting the next event from the queue");
            }

            return _nextEvent;
        }

        /// <summary>
        /// Provides functionality to peek at the next event in the queue
        /// </summary>
        public Event Peek()
        {
            if (_eventQueue.IsEmpty)
            {
                return null;
            }

            if (!_eventQueue.TryPeek(out _nextEvent))
            {
                Console.WriteLine("Failed peeking at the next event in the queue");
            }

            return _nextEvent;
        }
    }
}