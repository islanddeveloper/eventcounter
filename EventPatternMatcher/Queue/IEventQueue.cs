﻿using RC.CodingChallenge.Contracts;

namespace RC.CodingChallenge.Queue
{
    /// <summary>
    /// Provides the interface for working with the queue of hardware events
    /// </summary>
    public interface IEventQueue
    {
        /// <summary>
        /// Returns the next event and removes it from the queue
        /// </summary>
        /// <returns>The next event in the queue</returns>
        Event GetNext();

        /// <summary>
        /// Returns the next event without removing it
        /// </summary>
        /// <returns>The next event in the queue</returns>
        Event Peek();

        /// <summary>
        /// Adds an Event to the queue
        /// </summary>
        /// <param name="deviceEvent">hardware device event</param>
        void Add(Event deviceEvent);
    }
}