﻿using System;

namespace RC.CodingChallenge.Contracts
{
    /// <summary>
    /// A class to describe an event of a hardware device
    /// </summary>
    public class Event
    {
        /// <summary>
        /// Gets or sets the identifier of the device
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the date of an event
        /// </summary>
        public DateTime EventDate { get; set; }

        /// <summary>
        /// Gets or sets the event type
        /// </summary>
        public int EventType { get; set; }

        /// <summary>
        /// Gets or sets the possibility of a device fault
        /// </summary>
        public bool IsPossibleDeviceFault { get; set; }
    }
}